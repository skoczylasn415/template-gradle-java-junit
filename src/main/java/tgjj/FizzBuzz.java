/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tgjj;

public class FizzBuzz {
    public String converter(int param) {

      if (param < 0) {
            throw new IllegalArgumentException("Argument: " + param + " must be positive");
      }

      String paramString = new String();
      paramString += param;
      if (param %3 == 0 && param%5 == 0){
        return "FizzBuzz";
      }
      else if (param % 3 ==0){
        return "Fizz";
      }
      else if (param % 5 == 0){
        return "Buzz";
      }
      return paramString;
    }
}
